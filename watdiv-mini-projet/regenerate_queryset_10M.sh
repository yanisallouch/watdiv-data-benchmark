for n in {0..100};  
do
for qt in testsuite/templates/*.sparql-template; 
do
   qt2=${qt##*templates/}
   bin/Release/watdiv -q model/wsdbm-data-model.txt ${qt} 10000 1 > testsuite/queries/10M/${qt2%.sparql-template}_${n}_10M.queryset ;
done;
done;
cat testsuite/queries/10M/* > testsuite/queries/10M/10M.queryset
echo "Genererated :"
cat testsuite/queries/10M/10M.queryset| grep SELECT | wc -l
