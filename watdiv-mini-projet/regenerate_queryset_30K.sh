for qt in testsuite/templates/*.sparql-template; 
do
   qt2=${qt##*templates/}
   bin/Release/watdiv -q model/wsdbm-data-model.txt ${qt} 3000 1 > testsuite/queries/30K/${qt2%.sparql-template}_30K.queryset ;
done;
cat testsuite/queries/30K/* > testsuite/queries/30K/30K.queryset
echo "Genererated :"
cat testsuite/queries/30K/30K.queryset | grep SELECT | wc -l
