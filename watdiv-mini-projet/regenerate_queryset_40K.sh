for qt in testsuite/templates/*.sparql-template; 
do
   qt2=${qt##*templates/}
   bin/Release/watdiv -q model/wsdbm-data-model.txt ${qt} 4000 1 > testsuite/queries/40K/${qt2%.sparql-template}_40K.queryset ;
done;
cat testsuite/queries/40K/* > testsuite/queries/40K/40K.queryset
echo "Genererated :"
cat testsuite/queries/40K/40K.queryset | grep SELECT | wc -l
