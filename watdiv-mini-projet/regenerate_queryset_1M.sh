for n in {0..100};  
do
for qt in testsuite/templates/*.sparql-template; 
do
   qt2=${qt##*templates/}
   bin/Release/watdiv -q model/wsdbm-data-model.txt ${qt} 1000 1 > testsuite/queries/1M/${qt2%.sparql-template}_${n}_1M.queryset ;
done;
done;
cat testsuite/queries/1M/* > testsuite/queries/1M/1M.queryset
echo "Genererated :"
cat testsuite/queries/1M/1M.queryset | grep SELECT | wc -l
