for qt in testsuite/templates/*.sparql-template; 
do
   qt2=${qt##*templates/}
   bin/Release/watdiv -q model/wsdbm-data-model.txt ${qt} 1 1 > testsuite/queries/10/${qt2%.sparql-template}_10.queryset ;
done;
cat testsuite/queries/10/* > testsuite/queries/10/10.queryset
echo "Genererated :"
cat testsuite/queries/10/10.queryset | grep SELECT | wc -l
