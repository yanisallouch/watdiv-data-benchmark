for qt in testsuite/templates/*.sparql-template; 
do
   qt2=${qt##*templates/}
   bin/Release/watdiv -q model/wsdbm-data-model.txt ${qt} 2000 1 > testsuite/queries/20K/${qt2%.sparql-template}_20K.queryset ;
done;
cat testsuite/queries/20K/* > testsuite/queries/20K/20K.queryset
echo "Genererated :"
cat testsuite/queries/20K/20K.queryset | grep SELECT | wc -l
