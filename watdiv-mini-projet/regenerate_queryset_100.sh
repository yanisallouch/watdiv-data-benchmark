for qt in testsuite/templates/*.sparql-template; 
do
   qt2=${qt##*templates/}
   bin/Release/watdiv -q model/wsdbm-data-model.txt ${qt} 10 1 > testsuite/queries/100/${qt2%.sparql-template}_100.queryset ;
done;
cat testsuite/queries/100/* > testsuite/queries/100/100.queryset
echo "Genererated :"
cat testsuite/queries/100/100.queryset | grep SELECT | wc -l
