for qt in testsuite/templates/*.sparql-template; 
do
   qt2=${qt##*templates/}
   bin/Release/watdiv -q model/wsdbm-data-model.txt ${qt} 100 1 > testsuite/queries/1K/${qt2%.sparql-template}_1K.queryset ;
done;
cat testsuite/queries/1K/* > testsuite/queries/1K/1K.queryset
echo "Genererated :"
cat testsuite/queries/1K/1K.queryset | grep SELECT | wc -l
