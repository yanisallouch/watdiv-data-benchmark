dataExt=".nt"
queryExt=".queryset"
# workload = data + queries
dataFiles=("500K");
queriesFiles=("5K" "15K");
# jvm ram
ramUnit="g"
ramValues=("2" "4");

for dataFile in ${dataFiles[@]}; do
	data=$dataFile$dataExt;
	for queryFile in ${queriesFiles[@]}; do
		queries=$queryFile$queryExt;
		for ramValue in ${ramValues[@]}; do
			for i in {1..10}; do
				echo "java -Xmx$ramValue$ramUnit -jar qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar -s 1 -data $data -queries $queries";
				java -Xmx$ramValue$ramUnit -jar qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar -s 1 -data $data -queries $queries;
				echo "java -Xmx$ramValue$ramUnit -jar qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar -s 2 -data $data -queries $queries";
				java -Xmx$ramValue$ramUnit -jar qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar -s 2 -data $data -queries $queries;
			done
			rm -rf results/benchmarkInfo results/jenaQueriesResults results/qEngineQueriesResults;
			echo "java -jar qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar -s 5 --results results/qEngineMetricsResults";
			java -jar qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar -s 5 --results results/qEngineMetricsResults;
			echo "java -jar qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar -s 5 --results results/JenaMetricsResults";
			java -jar qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar -s 5 --results results/JenaMetricsResults;
			newFolder="benchmark-${ramValue}_${ramUnit}-${dataFile}_d-${queryFile}_q";
			mv results $newFolder;
			rm -rf results;
		done
	done
done