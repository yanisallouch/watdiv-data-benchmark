# Explication de l'arborescence

- Dans le dossier [samples](./samples), il y a des fichiers de test utiliser au debut du projet fournit sur le moodle du mobule HAI914I,
- Dans le dossier [watdiv-mini-projet](./watdiv-mini-projet), c'est le programme WatDiv qui permet de generer des donnees et des requetes randomises avec des scripts supplementaires pour influencer la quantite generer,
- Dans le dossier [benchmarks](./benchmarks), vous trouverer :
 - un jar nomme [qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar](./benchmarks/qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar). Ce jar permet de lancer :
  - notre implementation du moteur RDF,
  - notre implementation avec Jena Apache,
  - de generer un queryset optimise,
  - de verifier la completude et la correction de notre moteur en utilisant Jena Apache,
  - de parser un dossier results contenant plusieurs executions de notre moteur ou de jena et de calculer la moyenne robuste.
 - des fichiers au format **queryset** (requetes) ou **nt** (data) qui serviront a lancer le benchmark,
 - un script nomme [benchmark.sh](./benchmarks/benchmark.sh) qui va executer le benchmarking de notre moteur contre Jena Apache :
 - Un dossier commencant par "benchmark-" contient 4 sous dossiers, par exemple pour [benchmark-2_g-500K_d-15K_q](./benchmarks/benchmark-2_g-500K_d-15K_q) :
  - [JenaMetricsResults](./benchmarks/benchmark-2_g-500K_d-15K_q/JenaMetricsResults) qui contient 10 executions de notre implementation en Jena Apache,
  - [qEngineMetricsResults](./benchmarks/benchmark-2_g-500K_d-15K_q/qEngineMetricsResults) qui contient 10 executions de notre implementation d'un moteur RDF,
  - [jenaMetricsAvg](./benchmarks/benchmark-2_g-500K_d-15K_q/jenaMetricsAvg) qui contient un fichier CSV resultant du calcul de la moyenne robuste de 10 executions contenu dans le dossier [JenaMetricsResults](./benchmarks/benchmark-2_g-500K_d-15K_q/JenaMetricsResults),
  - [QEngineMetricsAvg](./benchmarks/benchmark-2_g-500K_d-15K_q/QEngineMetricsAvg) qui contient un fichier CSV resultant du calcul de la moyenne robuste de 10 executions contenu dans le dossier [qEngineMetricsResults](./benchmarks/benchmark-2_g-500K_d-15K_q/qEngineMetricsResults),

# Instructions pour executer le script de benchmark

Depuis un terminal Bash,

1. Se deplacer dans le dossier [benchmarks](./benchmarks),
2. Verifier la presence dans le repertoire courant des fichiers suivants :
 - [benchmark.sh](./benchmarks/benchmark.sh),
 - [qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar](./benchmarks/qengine-0.0.1-SNAPSHOT-jar-with-dependencies.jar),
 - [500K.nt](./benchmarks/500K.nt)
 - [5K.queryset](./benchmarks/5K.queryset)
 - [15K.queryset](./benchmarks/15K.queryset)
3. Verifier que vous disposer de Java JDK 1.8 en CLI depuis le repertoire courant,
4. Executer le script [benchmark.sh](./benchmarks/benchmark.sh) avec la commande `./benchmark.sh`

# Ou sont les resultats ?

Les resultats servant a faire les histogrammes (a la main!) sont contenus dans les sous-dossiers d'un benchmark, par exemple pour le benchmark [benchmark-2_g-500K_d-15K_q](./benchmarks/benchmark-2_g-500K_d-15K_q) :

- [jenaMetricsAvg](./benchmarks/benchmark-2_g-500K_d-15K_q/jenaMetricsAvg) qui contient un fichier CSV resultant du calcul de la moyenne robuste de 10 executions contenu dans le dossier [JenaMetricsResults](./benchmarks/benchmark-2_g-500K_d-15K_q/JenaMetricsResults),
- [QEngineMetricsAvg](./benchmarks/benchmark-2_g-500K_d-15K_q/QEngineMetricsAvg) qui contient un fichier CSV resultant du calcul de la moyenne robuste de 10 executions contenu dans le dossier [qEngineMetricsResults](./benchmarks/benchmark-2_g-500K_d-15K_q/qEngineMetricsResults),

> Remarque : L'execution d'une experience 2^2 avec les donnees fournit dans ce depot, dure environ 15 minutes sur une machine avec un AMD Ryzen 5 2600X OC et 32G de ram sous Ubuntu 20.04 LTS.